import time
import torch
import torch.nn as nn
import torch.optim as optim
import torch.nn.functional as F
import torchvision
import torchvision.transforms as transforms
from torch.utils.data import DataLoader,Dataset
import matplotlib.pyplot as plt
import PIL.Image as Image
import cv2
import numpy as np
import os
import sys
import argparse
import random
import json
import copy
import shutil

class RandomHFlip:
    def __init__(self,p=0.5):
        self.p = p
    
    def __call__(self,img,template):
        if random.random() < self.p:
            img = img.transpose(Image.FLIP_LEFT_RIGHT)
            template = template.transpose(Image.FLIP_LEFT_RIGHT)
        return img,template
    
class RandomVFlip:
    def __init__(self,p=0.5):
        self.p = p
    
    def __call__(self,img,template):
        if random.random() < self.p:
            img = img.transpose(Image.FLIP_TOP_BOTTOM)
            template = template.transpose(Image.FLIP_TOP_BOTTOM)
        return img,template
    
class RandomRotate90:
    def __init__(self,p=0.5):
        self.p = p
    
    def __call__(self,img,template):
        if random.random() < self.p:
            img = img.transpose(Image.ROTATE_90)
            template = template.transpose(Image.ROTATE_90)
        return img,template
    
class RandomRotate180:
    def __init__(self,p=0.5):
        self.p = p
    
    def __call__(self,img,template):
        if random.random() < self.p:
            img = img.transpose(Image.ROTATE_180)
            template = template.transpose(Image.ROTATE_180)
        return img,template

class Compose:
    def __init__(self,transforms):
        self.transforms = transforms
    
    def __call__(self,img,template):
        for t in self.transforms:
            img,template = t(img,template)
        return img,template

class FlawClsDataset(Dataset):
    def __init__(self,split):
        self.path = "../flaw_data/"+split+"/"+split+"_cls.txt"
        self.lines = open(self.path).readlines()
        self.split = split
        self.transform = transforms.Compose([
            transforms.Resize((400,400)),
            transforms.ToTensor(),
            transforms.Normalize((0.5,0.5,0.5),(0.5,0.5,0.5))
        ])
        self.augmentation = Compose([
            RandomHFlip(),
            RandomVFlip(),
            RandomRotate90(),
            RandomRotate180()
        ])

        self.files = []
        self.cls = []
        for line in self.lines:
            line = line.strip().split(",")
            self.files.append(line[0])
            self.cls.append(int(line[1]))

    def __getitem__(self,index):
        path_img = "../flaw_data/"+self.split+"/sample/"+self.files[index]
        path_template = "../flaw_data/"+self.split+"/template/"+self.files[index].replace(".jpg","_template.jpg")
        img = Image.open(path_img)
        template = Image.open(path_template)

        if self.split == "train":
            img,template = self.augmentation(img,template)
            
        img = self.transform(img)
        template = self.transform(template)
        cls = self.cls[index]
        fused_img = torch.cat((img,template),0)
        return fused_img,cls
    
    def __len__(self):
        return len(self.files)
    

class FlawClsNet(nn.Module):
    def __init__(self):
        super(FlawClsNet,self).__init__()
        self.resnet_base = torchvision.models.resnet50(pretrained=True)
        self.resnet_base.fc = nn.Linear(2048,14)
        self.resnet_base.conv1 = nn.Conv2d(6,64,kernel_size=7,stride=2,padding=3,bias=False)
    
    def forward(self,x):
        x = self.resnet_base(x)
        return x
    
class FlawClsSiamDiffNet(nn.Module):
    def __init__(self):
        self.resnet_base = torchvision.models.resnet50(pretrained=True)
        self.feature_extractor = self.resnet_base.features

def append_log(log_path,log):
    with open(log_path,"a") as f:
        f.write(log+"\n")

class AvgMeter:
    def __init__(self):
        self.reset()
    
    def reset(self):
        self.avg = 0
        self.sum = 0
        self.count = 0
    
    def update(self,val,n=1):
        self.sum += val*n
        self.count += n
        self.avg = self.sum/self.count


def train_one_epoch(model,optimizer,train_loader,device,epoch):
    model.train()
    true_count = 0
    time_start = time.time()
    avg_loss = AvgMeter()
    avg_acc = AvgMeter()

    for batch_idx,(data,cls) in enumerate(train_loader):
        data,cls = data.to(device),cls.to(device)
        optimizer.zero_grad()
        output = model(data)
        loss = F.cross_entropy(output,cls)
        loss.backward()
        optimizer.step()
        pred = output.max(1,keepdim=True)[1]
        true_count += pred.eq(cls.view_as(pred)).sum().item()

        avg_loss.update(loss.item(),data.size(0))
        avg_acc.update(pred.eq(cls.view_as(pred)).sum().item()/data.size(0),data.size(0))

        if batch_idx % 10 == 0:
            print("Train Epoch: {} [{}/{} ({:.0f}%)]\tEta:{}s\tLoss:{:.6f}\tAccuracy:{:.6f}".format(
                epoch,batch_idx,len(train_loader),
                100.*batch_idx/len(train_loader),
                str((time.time()-time_start)/(batch_idx+1)*(len(train_loader)-batch_idx-1))[:7],
                avg_loss.avg,avg_acc.avg
            ))
    print("Complete, time cost: {}s".format(time.time()-time_start))
    append_log("../CLSOutput/flaw_cls_train.log","Epoch:{}\tLoss:{:.6f}\tAccuracy:{:.6f}".format(
        epoch,avg_loss.avg,avg_acc.avg
    ))


def evaluate(model,test_loader,device):
    model.eval()
    test_loss = 0
    correct = 0
    avg_loss = AvgMeter()
    avg_acc = AvgMeter()
    with torch.no_grad():
        for data,cls in test_loader:
            data,cls = data.to(device),cls.to(device)
            output = model(data)
            test_loss += F.cross_entropy(output,cls,reduction="sum").item()
            pred = output.max(1,keepdim=True)[1]
            correct += pred.eq(cls.view_as(pred)).sum().item()
            avg_loss.update(test_loss,data.size(0))
            avg_acc.update(pred.eq(cls.view_as(pred)).sum().item()/data.size(0),data.size(0))
    
    test_loss /= len(test_loader.dataset)
    print("\nTest set: Average loss:{:.4f},Accuracy:{}/{} ({:.0f}%)\n".format(
        avg_loss.avg
        ,correct,len(test_loader.dataset),
        100.*correct/len(test_loader.dataset)
    ),flush=True)
    append_log("../CLSOutput/flaw_cls_test.log","Loss:{:.6f}\tAccuracy:{:.6f}".format(
        avg_loss.avg, 100.*correct/len(test_loader.dataset)
    ))

    return test_loss

def train(model,optimizer,train_loader,test_loader,device,epochs,scheduler=None):
    best_loss = 100
    for epoch in range(epochs):
        train_one_epoch(model,optimizer,train_loader,device,epoch)
        test_loss = evaluate(model,test_loader,device)
        if test_loss < best_loss:
            best_loss = test_loss
            torch.save(model.state_dict(),"../CLSOutput/flaw_cls_best.pth")
            print("model saved")
        torch.save(model.state_dict(),"../CLSOutput/flaw_cls_last.pth")
        torch.save(model.state_dict(),"../CLSOutput/flaw_cls_epoch_"+str(epoch)+".pth")
        if scheduler is not None:
            scheduler.step()


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("--batch_size",type=int,default=16)
    parser.add_argument("--epochs",type=int,default=100)
    parser.add_argument("--lr",type=float,default=0.002)
    parser.add_argument("--gpu",type=int,default=0)
    args = parser.parse_args()
    print(args)

    device = torch.device("cuda:"+str(args.gpu) if torch.cuda.is_available() else "cpu")
    train_dataset = FlawClsDataset("train")
    test_dataset = FlawClsDataset("test")
    train_loader = DataLoader(train_dataset,batch_size=args.batch_size,shuffle=True,num_workers=4)
    test_loader = DataLoader(test_dataset,batch_size=args.batch_size,shuffle=False,num_workers=4)
    model = FlawClsNet().to(device)

    optimizer = optim.Adam(model.parameters(),lr=args.lr)
    scheduler = optim.lr_scheduler.StepLR(optimizer,step_size=30,gamma=0.1)
    train(model,optimizer,train_loader,test_loader,device,args.epochs,scheduler)

if __name__ == "__main__":
    main()