import time
from typing import Any
import torch
import torch.nn as nn
import torch.optim as optim
import torch.nn.functional as F
import torchvision
import torchvision.transforms as transforms
from torch.utils.data import DataLoader,Dataset
import matplotlib.pyplot as plt
import PIL.Image as Image
import cv2
import numpy as np
import os
import sys
import argparse
import random
import json
import copy
import shutil

import torch
from torch import nn
import torch.nn.functional as F
from torch.nn.modules.padding import ReplicationPad2d
import segmentation_models_pytorch as smp
from segmentation_models_pytorch.encoders import get_preprocessing_fn
from segmentation_models_pytorch.losses import DiceLoss, FocalLoss
from segmentation_models_pytorch import Unet,UnetPlusPlus,FPN
from PIL import ImageFilter
import PIL.ImageTransform as ImageTransform
from mod_unet_diff import UnetMdDiff

from mod_unet import UnetMd
import albumentations as albu

from mod_changer import ChangerNet
from mod_segformer import SegFormer
from mod_fpn import FPNDiff

class UNetWrapper(nn.Module):
    def __init__(self) -> None:
        super().__init__()
        self.unet = Unet('resnet34', encoder_weights='imagenet', classes=2, activation=None, in_channels=6, decoder_channels=(128,64,32,16),encoder_depth=4)
    
    def forward(self, x, y):
        x = torch.cat([x, y], dim=1)
        return self.unet(x)
    
class UNetppWrapper(nn.Module):
    def __init__(self) -> None:
        super().__init__()
        self.unet = UnetPlusPlus('resnet34', encoder_weights='imagenet', classes=2, activation=None, in_channels=6, decoder_channels=(256,128,64,32,16),encoder_depth=5)
    
    def forward(self, x, y):
        x = torch.cat([x, y], dim=1)
        return self.unet(x)
    
class SegFormerWrapper(nn.Module):
    def __init__(self) -> None:
        super().__init__()
        self.unet = SegFormer('MiT-B1')
    
    def forward(self, x, y):
        x = torch.cat([x, y], dim=1)
        return self.unet(x)
    
class UNetWrapper_Single(nn.Module):
    def __init__(self) -> None:
        super().__init__()
        self.unet = Unet('resnet34', encoder_weights='imagenet', classes=2, activation=None, in_channels=3, decoder_channels=(256,128,64,32,16),encoder_depth=5)
    
    def forward(self, x, y):
        #x = torch.cat([x, y], dim=1)
        return self.unet(x)
    
class FPNWrapper(nn.Module):
    def __init__(self) -> None:
        super().__init__()
        self.unet = FPN('resnet34', encoder_weights='imagenet', classes=2, activation=None, in_channels=6)

    def forward(self, x, y):
        x = torch.cat([x, y], dim=1)
        return self.unet(x)

class RandomHFlip:
    def __init__(self):
        pass
    
    def __call__(self,img,template,mask):
        if random.random() > 0.5:
            img = img.transpose(Image.Transpose.FLIP_LEFT_RIGHT)
            template = template.transpose(Image.Transpose.FLIP_LEFT_RIGHT)
            mask = mask.transpose(Image.Transpose.FLIP_LEFT_RIGHT)
        return img,template,mask
    
class RandomVFlip:
    def __init__(self):
        pass
    
    def __call__(self,img,template,mask):
        if random.random() > 0.5:
            img = img.transpose(Image.Transpose.FLIP_TOP_BOTTOM)
            template = template.transpose(Image.Transpose.FLIP_TOP_BOTTOM)
            mask = mask.transpose(Image.Transpose.FLIP_TOP_BOTTOM)
        return img,template,mask
    
class RandomRotate90:
    def __init__(self):
        pass
    
    def __call__(self,img,template,mask):
        if random.random() > 0.5:
            img = img.transpose(Image.Transpose.ROTATE_90)
            template = template.transpose(Image.Transpose.ROTATE_90)
            mask = mask.transpose(Image.Transpose.ROTATE_90)
        return img,template,mask
    
class RandomRotate180:
    def __init__(self):
        pass
    
    def __call__(self,img,template,mask):
        if random.random() > 0.5:
            img = img.transpose(Image.Transpose.ROTATE_180)
            template = template.transpose(Image.Transpose.ROTATE_180)
            mask = mask.transpose(Image.Transpose.ROTATE_180)
        return img,template,mask

class RandomCrop:
    def __init__(self,size,p):
        self.size = size
        self.p = p
    
    def __call__(self,img,template,mask):
        if random.random() > 1-self.p:
            w,h = img.size
            th,tw = self.size
            if w == tw and h == th:
                return img,template,mask
            if w < tw or h < th:
                return img,template,mask
            x1 = random.randint(0,w-tw)
            y1 = random.randint(0,h-th)
            img = img.crop((x1,y1,x1+tw,y1+th))
            template = template.crop((x1,y1,x1+tw,y1+th))
            mask = mask.crop((x1,y1,x1+tw,y1+th))
        return img,template,mask    

class ColorJitter:
    def __init__(self,brightness=0.25,contrast=0.25,saturation=0.25,hue=0.25):
        self.brightness = brightness
        self.contrast = contrast
        self.saturation = saturation
        self.hue = hue

        
    
    def __call__(self,img,template,mask):
        if random.random() > 0.8:
            cj = albu.ColorJitter(brightness=self.brightness,contrast=self.contrast,saturation=self.saturation,hue=self.hue)
            rp = cj(image=np.array(img),image0=np.array(template))
            img, template = rp["image"],rp["image0"]
            img = Image.fromarray(img)
            template = Image.fromarray(template)
        return img,template,mask


class Compose:
    def __init__(self,transforms):
        self.transforms = transforms
    
    def __call__(self,img,template,mask):
        for t in self.transforms:
            img,template,mask = t(img,template,mask)
        return img,template,mask
    
class RandomSelect:
    def __init__(self,transforms,p=1):
        self.transforms = transforms
        self.p = p
    
    def __call__(self,img,template,mask):
        if random.random() < self.p:
            t = random.choice(self.transforms)
            img,template,mask = t(img,template,mask)
        return img,template,mask
    
class RandomRotate:
    def __init__(self):
        self.rot = albu.Rotate(limit=7,interpolation=cv2.INTER_CUBIC,border_mode=cv2.BORDER_REFLECT_101,)
    
    def __call__(self,img,template,mask):
        if random.random() > 0.7:
            rp = self.rot(image=np.array(img),image0=np.array(template),mask=np.array(mask))
            img,template,mask = rp["image"],rp["image0"],rp["mask"]
            img = Image.fromarray(img)
            template = Image.fromarray(template)
            mask = Image.fromarray(mask)
        return img,template,mask

class FlawSegDataset(Dataset):
    def __init__(self,split):
        self.path = "../flaw_data/"+split+"/"+split+"_cls.txt"
        self.lines = open(self.path).readlines()
        self.split = split
        self.transform = transforms.Compose([
            #transforms.Resize((416*2,416*2)), #cHANGER
            transforms.Resize((416,416)),
            transforms.ToTensor(),
            transforms.Normalize([0.485,0.456,0.406],[0.229,0.224,0.225])
        ])
        
        self.augmentation = Compose([
            RandomHFlip(),
            RandomVFlip(),
            RandomRotate90(),
            RandomRotate180(),
            RandomSelect([
                RandomCrop((300,300),0.25),
                RandomCrop((350,350),0.5),
                RandomCrop((250,250),0.25),
                RandomCrop((100,100),0.15),
            ]),
            ColorJitter(),
            RandomRotate()
        ])
        
        self.mask_transform = transforms.Compose([
            transforms.Resize((416,416)),
            transforms.ToTensor()
        ])

        self.files = []
        self.templates = []
        self.mask = []
        p = 0
        for line in self.lines:
            line = line.strip().split(",")
            if os.path.exists("../flaw_data/"+self.split+"/mask/"+line[0].replace(".jpg","_mask.jpg")):
                self.files.append(line[0])
                self.templates.append(line[0].replace(".jpg","_template.jpg"))
                self.mask.append(line[0].replace(".jpg","_mask.jpg"))
            else:
                print("No Mask:",line[0], " I=",p)
                p+=1

        print("Split:",self.split,"Len:",len(self.files))

    def __getitem__(self,index):
        path_img = "../flaw_data/"+self.split+"/sample/"+self.files[index]
        path_template = "../flaw_data/"+self.split+"/template/"+self.templates[index]
        path_mask = "../flaw_data/"+self.split+"/mask/"+self.mask[index]
        img = Image.open(path_img)
        template = Image.open(path_template)
        mask = Image.open(path_mask)

        #print(path_img)

        if self.split == "train":
            img,template,mask = self.augmentation(img,template,mask)

        img = self.transform(img)
        template = self.transform(template)
        mask = self.mask_transform(mask)
        
        mask[mask>0.5] = 1
        mask[mask<=0.5] = 0

        # Make mask one hot
        mask = torch.cat((1-mask,mask),dim=0)
        
        return img,template,mask

    def __len__(self):
        return len(self.files)
    

class MaskIoULoss(nn.Module):
    def __init__(self):
        super(MaskIoULoss,self).__init__()
    
    def forward(self,pred,mask):
        #pred = torch.sigmoid(pred)
        pred = torch.softmax(pred,dim=1)
        pred = pred.view(-1)
        mask = mask.view(-1)
        intersection = (pred*mask).sum()
        union = pred.sum() + mask.sum() - intersection
        loss = 1 - (intersection + 1)/(union + 1)
        return loss

class MaskIoUForeLoss(nn.Module):
    def __init__(self):
        super(MaskIoUForeLoss,self).__init__()
    
    def forward(self,pred,mask):
        pred = torch.softmax(pred,dim=1)
        pred = pred[1].view(-1)
        mask = mask[1].view(-1)
        intersection = (pred*mask).sum()
        union = pred.sum() + mask.sum() - intersection
        loss = 1 - (intersection + 1)/(union + 1)
        return loss

class MaskBCELoss(nn.Module):
    def __init__(self):
        super(MaskBCELoss,self).__init__()
    
    def forward(self,pred,mask):
        pred = torch.softmax(pred,dim=1)
        pred = pred.view(-1)
        mask = mask.view(-1)
        loss = F.binary_cross_entropy(pred,mask)
        return loss
    
class MaskCELoss(nn.Module):
    def __init__(self):
        super(MaskCELoss,self).__init__()
    
    def forward(self,pred,mask):
        pred = torch.softmax(pred,dim=1)
        pred = pred.view(-1)
        mask = mask.view(-1)
        loss = F.cross_entropy(pred,mask)
        return loss
    
class SegLoss():
    def __init__(self):
        self.loss = DiceLoss(mode='multiclass')
        self.loss2 = FocalLoss(mode='multiclass')
        self.loss3 = DiceLoss(mode='multiclass',ignore_index=0)
    
    def __call__(self,pred,mask):
        # Onehot to label
        mask = mask.argmax(dim=1)
        focal = self.loss2(pred,mask)
        dice = self.loss(pred,mask)
        dice_fore = self.loss3(pred,mask) * 1.5
        return focal+dice+dice_fore, focal,dice
     
    
class AverageMeter:
    def __init__(self):
        self.reset()
    
    def reset(self):
        self.val = 0
        self.avg = 0
        self.sum = 0.0
        self.count = 0
    
    def update(self,val,n=1):
        self.val = val
        self.sum += val*n
        self.count += n
        self.avg = self.sum / self.count

    def get(self):
        return self.avg

def _take_channels(*xs, ignore_channels=None):
    if ignore_channels is None:
        return xs
    else:
        channels = [channel for channel in range(xs[0].shape[1]) if channel not in ignore_channels]
        xs = [torch.index_select(x, dim=1, index=torch.tensor(channels).to(x.device)) for x in xs]
        return xs
    

def mask_acc_score(mask_pr,mask_gt):
    mask_pr = mask_pr.view(-1)
    mask_gt = mask_gt.view(-1)
    mask_pr[mask_pr>0.5] = 1
    mask_pr[mask_pr<=0.5] = 0
    mask_gt[mask_gt>0.5] = 1
    mask_gt[mask_gt<=0.5] = 0
    return (mask_pr==mask_gt).sum().item()/mask_gt.shape[0]

def mask_acc_foreground_score(mask_pr,mask_gt):
    mask_pr = mask_pr[1].view(-1)
    mask_gt = mask_gt[1].view(-1)
    mask_pr[mask_pr>0.5] = 1
    mask_pr[mask_pr<=0.5] = 0
    mask_gt[mask_gt>0.5] = 1
    mask_gt[mask_gt<=0.5] = 0
    return (mask_pr*mask_gt).sum().item()/mask_gt.sum()

def mask_precision_score(mask_pr,mask_gt):
    mask_pr = mask_pr.view(-1)
    mask_gt = mask_gt.view(-1)
    mask_pr[mask_pr>0.5] = 1
    mask_pr[mask_pr<=0.5] = 0
    mask_gt[mask_gt>0.5] = 1
    mask_gt[mask_gt<=0.5] = 0
    intersection = (mask_pr*mask_gt).sum()
    return intersection/mask_pr.sum()

def mask_precision_foreground_score(mask_pr,mask_gt):
    mask_pr = mask_pr[1].view(-1)
    mask_gt = mask_gt[1].view(-1)
    mask_pr[mask_pr>0.5] = 1
    mask_pr[mask_pr<=0.5] = 0
    mask_gt[mask_gt>0.5] = 1
    mask_gt[mask_gt<=0.5] = 0
    intersection = (mask_pr*mask_gt).sum()
    return (intersection+1e-6)/(mask_pr.sum()+1e-6)

def mask_iou_score(mask_pr,mask_gt):
    def iou(pr, gt, eps=1e-7, threshold=None, ignore_channels=None):
        pr, gt = _take_channels(pr, gt, ignore_channels=ignore_channels)

        intersection = torch.sum(gt * pr)
        union = torch.sum(gt) + torch.sum(pr) - intersection + eps
        return (intersection + eps) / union

    return iou(mask_pr,mask_gt)

def mask_iou_foreground_score(mask_pr,mask_gt):
    def iou(pr, gt, eps=1e-7, threshold=None, ignore_channels=None):
        intersection = torch.sum(gt * pr)
        union = torch.sum(gt) + torch.sum(pr) - intersection + eps
        return (intersection + eps) / union

    return iou(mask_pr[1],mask_gt[1])


def make_metrics(mask_pr,mask_gt):
    # Make shape BCHW
    mask_pr = mask_pr.view(-1,2,416,416)
    mask_gt = mask_gt.view(-1,2,416,416)
    tp, fp, fn, tn = smp.metrics.get_stats(mask_pr, mask_gt, mode='multiclass')

    iou_score = smp.metrics.iou_score(tp, fp, fn, tn, reduction="micro")
    f1_score = smp.metrics.f1_score(tp, fp, fn, tn, reduction="micro")
    f2_score = smp.metrics.fbeta_score(tp, fp, fn, tn, beta=2, reduction="micro")
    accuracy = smp.metrics.accuracy(tp, fp, fn, tn, reduction="macro")
    recall = smp.metrics.recall(tp, fp, fn, tn, reduction="micro-imagewise")
    return iou_score,f1_score,f2_score,accuracy,recall

def make_metrics_foreground(mask_pr,mask_gt):
    # Make shape BCHW
    mask_pr = mask_pr.view(-1,2,416,416)
    mask_gt = mask_gt.view(-1,2,416,416)
    tp, fp, fn, tn = smp.metrics.get_stats(mask_pr, mask_gt, mode='multiclass')

    iou_score = smp.metrics.iou_score(tp, fp, fn, tn, reduction="micro",ignore_channels=[0])
    f1_score = smp.metrics.f1_score(tp, fp, fn, tn, reduction="micro",ignore_channels=[0])
    f2_score = smp.metrics.fbeta_score(tp, fp, fn, tn, beta=2, reduction="micro",ignore_channels=[0])
    accuracy = smp.metrics.accuracy(tp, fp, fn, tn, reduction="macro",ignore_channels=[0])
    recall = smp.metrics.recall(tp, fp, fn, tn, reduction="micro-imagewise",ignore_channels=[0])
    return iou_score,f1_score,f2_score,accuracy,recall


def mask_dice_score(mask_pr,mask_gt):
    mask_pr = mask_pr.view(-1)
    mask_gt = mask_gt.view(-1)
    mask_pr[mask_pr>0.5] = 1
    mask_pr[mask_pr<=0.5] = 0
    mask_gt[mask_gt>0.5] = 1
    mask_gt[mask_gt<=0.5] = 0
    intersection = (mask_pr*mask_gt).sum()
    return (2*intersection+1)/(mask_pr.sum()+mask_gt.sum()+1)

def append_log(log_path,log):
    with open(log_path,"a") as f:
        f.write(log+"\n")

def train_one_epoch(model,optimizer,criterion,train_loader,device,epoch):
    model.train()
    true_count = 0
    time_start = time.time()
    
    avg_dice = AverageMeter()
    avg_iou = AverageMeter()
    avg_accuracy = AverageMeter()
    avg_loss = AverageMeter()
    avg_foreground_iou = AverageMeter()

    avg_dice_loss = AverageMeter()
    avg_focal_loss = AverageMeter()

    time_start = time.time()
    for batch_idx,(data,template,mask) in enumerate(train_loader):
        data,template,mask = data.to(device),template.to(device),mask.to(device)
        optimizer.zero_grad()
        output = model(data,template)
        loss,loss_focal,loss_dice = criterion(output,mask)
        loss.backward()
        optimizer.step()

        for b in range(data.shape[0]):
            pred_mask = torch.softmax(output[b],dim=0)
            dice = mask_dice_score(pred_mask,mask[b]).detach().cpu().numpy()
            iou = mask_iou_score(pred_mask,mask[b]).detach().cpu().numpy()
            acc = mask_acc_score(pred_mask,mask[b])
            foreground_iou = mask_iou_foreground_score(pred_mask,mask[b]).detach().cpu().numpy()

            avg_dice.update(dice)
            avg_iou.update(iou)
            avg_accuracy.update(acc)
            avg_loss.update(loss.item())
            avg_foreground_iou.update(foreground_iou)
            avg_dice_loss.update(loss_dice.item())
            avg_focal_loss.update(loss_focal.item())


        if batch_idx % 10 == 0:
            print("Epoch:",epoch,"Batch:",batch_idx,"/",len(train_loader),
                  "Eta:",(time.time()-time_start)/(batch_idx+1)*(len(train_loader)-batch_idx-1),
                  "Loss:",avg_loss.get(),
                  #"Dice:",avg_dice.get(),
                  "IoU:",avg_iou.get(),
                  "ForeIoU:",avg_foreground_iou.get(),
                  "DiceLoss:",avg_dice_loss.get(),
                  "FocalLoss:",avg_focal_loss.get(),
                  "Acc:",avg_accuracy.get())
    append_log("../SEGOutput/seg_train.log","Epoch:{:d}\tLoss:{:.6f}\tIoU:{:.6f}\tForeIoU:{:.6f}\tAcc:{:.6f}".format(
        epoch,avg_loss.get(),avg_iou.get(),avg_foreground_iou.get(),avg_accuracy.get()
    ))
    time_end = time.time()
    print("Epoch:",epoch,"Time:",time_end-time_start)


def evaluate(model,criterion,test_loader,device):
    model.eval()
    avg_dice = AverageMeter()
    avg_iou = AverageMeter()
    avg_accuracy = AverageMeter()
    avg_loss = AverageMeter()
    avg_foreground_iou = AverageMeter()
    avg_loss_focal = AverageMeter()
    avg_loss_dice = AverageMeter()

    avg_acc_foreground = AverageMeter()
    avg_precision_foreground = AverageMeter()
    avg_dice_foreground = AverageMeter()

    avg_precision = AverageMeter()

    from tqdm import tqdm
    
    with torch.no_grad():
        for batch_idx,(data,template,mask) in tqdm(enumerate(test_loader)):
            data,template,mask = data.to(device),template.to(device),mask.to(device)
            output = model(data,template)
            loss,loss_focal,loss_dice = criterion(output,mask)
            for b in range(data.shape[0]):
                pred_mask = torch.softmax(output[b],dim=0)
                dice = mask_dice_score(pred_mask,mask[b]).detach().cpu().numpy()
                iou = mask_iou_score(pred_mask,mask[b]).detach().cpu().numpy()
                acc = mask_acc_score(pred_mask,mask[b])
                fore_iou = mask_iou_foreground_score(pred_mask,mask[b]).detach().cpu().numpy()

                fore_acc = mask_acc_foreground_score(pred_mask,mask[b])
                fore_precision = mask_precision_foreground_score(pred_mask,mask[b])
                fore_dice = mask_dice_score(pred_mask[1],mask[b][1]).detach().cpu().numpy()
                

                precision = mask_precision_score(pred_mask,mask[b])
                
                avg_dice.update(dice)
                avg_iou.update(iou)
                avg_accuracy.update(acc)
                avg_foreground_iou.update(fore_iou)
                avg_loss_dice.update(loss_dice.item())
                avg_loss_focal.update(loss_focal.item())
                avg_loss.update(loss.item())

                avg_acc_foreground.update(fore_acc)
                avg_precision_foreground.update(fore_precision)
                avg_dice_foreground.update(fore_dice)

                avg_precision.update(precision)
    print("Test:",
          "Loss:",avg_loss.get(),
          "IoU:",avg_iou.get(),
          "Dice:",avg_dice.get(),
          "ForeIoU:",avg_foreground_iou.get(),
          "DiceLoss:",avg_loss_dice.get(),
          "FocalLoss:",avg_loss_focal.get(),
          "Acc:",avg_accuracy.get(),
          "ForeAcc:",avg_acc_foreground.get(),
          "ForePrecision:",avg_precision_foreground.get(),
          "ForeDice:",avg_dice_foreground.get(),
        "Precision:",avg_precision.get())
    
    append_log("../SEGOutput/seg_test.log","Loss:{:.6f}\tIoU:{:.6f}\tForeIoU:{:.6f}\tAcc:{:.6f}".format(
        avg_loss.get(),avg_iou.get(),avg_foreground_iou.get(),avg_accuracy.get()
    ))
    return avg_loss.get(),avg_dice.get(),avg_iou.get(),avg_accuracy.get()

def train(model,optimizer,criterion,train_loader,test_loader,device,epochs,scheduler=None):
    best_dice = 0
    for epoch in range(epochs):
        train_one_epoch(model,optimizer,criterion,train_loader,device,epoch)
        loss,dice,iou,acc = evaluate(model,criterion,test_loader,device)
        if dice > best_dice:
            best_dice = dice
            torch.save(model.state_dict(),"../SEGOutput/seg_best.pth")
            print("Save best dice model")
        torch.save(model.state_dict(),"../SEGOutput/seg_last.pth")
        torch.save(model.state_dict(),"../SEGOutput/seg_epoch_"+str(epoch)+".pth")
        print("Save last model")
        print("Epoch:",epoch,"Loss:",loss,"Dice:",dice,"IoU:",iou,"Acc:",acc)
        if scheduler is not None:
            scheduler.step()

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("--batch_size",type=int,default=8)
    parser.add_argument("--epochs",type=int,default=250)
    parser.add_argument("--lr",type=float,default=2e-4)
    parser.add_argument("--device",type=str,default="cuda")
    parser.add_argument("--resume",type=str,default=None)
    parser.add_argument("--split",type=str,default="train")
    args = parser.parse_args()
    print(args)

    train_dataset = FlawSegDataset(args.split)
    train_loader = DataLoader(train_dataset,batch_size=args.batch_size,shuffle=True,num_workers=4)

    test_dataset = FlawSegDataset("test")
    test_loader = DataLoader(test_dataset,batch_size=args.batch_size,shuffle=False,num_workers=4)

    # model = UnetMd(classes=2,decoder_attention_type='scse',decoder_channels=(512,256,128,64,32)) # Save 4 & 5
    # model = ChangerNet(2)
    # model = UNetppWrapper()
    # model = SegFormerWrapper()
    # model = UNetWrapper_Single()
    # model = UnetMdDiff(classes=2,decoder_attention_type='scse',decoder_channels=(512,256,128,64,32))
    # model = FPNWrapper()
    #model = FPNDiff('resnet34', encoder_weights='imagenet', classes=2,decoder_segmentation_channels=256)
    model = UNetWrapper()

    model = model.to(args.device)
    criterion = SegLoss()
    optimizer = optim.Adam(model.parameters(),lr=args.lr)
    scheduler = optim.lr_scheduler.StepLR(optimizer,step_size=30,gamma=0.2)
    
    #model.load_state_dict(torch.load("/home/funkybirds/assignments/mlfin/SEGOutput_4A/seg_epoch_49.pth"))
    #print("Load model from", "/home/funkybirds/assignments/mlfin/SEGOutput_4A/seg_epoch_49.pth")
    train(model,optimizer,criterion,train_loader,test_loader,args.device,args.epochs,scheduler=scheduler)


def main_evaluate():
    parser = argparse.ArgumentParser()
    parser.add_argument("--batch_size",type=int,default=8)
    parser.add_argument("--epochs",type=int,default=250)
    parser.add_argument("--lr",type=float,default=2e-4)
    parser.add_argument("--device",type=str,default="cuda")
    parser.add_argument("--resume",type=str,default=None)
    parser.add_argument("--split",type=str,default="train")
    args = parser.parse_args()
    print(args)

    test_dataset = FlawSegDataset("test")
    test_loader = DataLoader(test_dataset,batch_size=args.batch_size,shuffle=False,num_workers=4)

    #model = FPNDiff('resnet34', encoder_weights='imagenet', classes=2,decoder_segmentation_channels=256)
    #model = UnetMdDiff(classes=2,decoder_attention_type='scse',decoder_channels=(512,256,128,64,32))
    # model = UNetWrapper_Single()
    # model = UnetMd(classes=2,decoder_attention_type='scse',decoder_channels=(512,256,128,64,32)) 
    model = SegFormerWrapper()
    model = model.to(args.device)
    criterion = SegLoss()

    model.load_state_dict(torch.load("/home/funkybirds/assignments/mlfin/SEGOutput_7_SegFormer/seg_last.pth"))
    evaluate(model,criterion,test_loader,args.device)



def test():
    test_dataset = FlawSegDataset("test")
    model = UnetMd(classes=2,decoder_attention_type='scse',decoder_channels=(512,256,128,64,32))
    model.load_state_dict(torch.load("/home/funkybirds/assignments/mlfin/SEGOutput/seg_last.pth"))

    img,template,mask = test_dataset[514]
    img = img.unsqueeze(0)
    template = template.unsqueeze(0)
    mask = mask.unsqueeze(0)

    output = model(img,template)
    pred_mask = torch.softmax(output,dim=1)
    print(pred_mask.shape)

    plt.subplot(2,2,1)
    plt.imshow(img[0].permute(1,2,0).detach().cpu().numpy())
    plt.title("Input")
    plt.subplot(2,2,2)
    plt.imshow(template[0].permute(1,2,0).detach().cpu().numpy())
    plt.title("Template")
    plt.subplot(2,2,3)
    plt.imshow(pred_mask[0][1].detach().cpu().numpy())
    plt.title("Pred")
    plt.subplot(2,2,4)
    plt.imshow(mask[0][1].detach().cpu().numpy())
    plt.title("Ground Truth")
    plt.show()


if __name__ == "__main__":
    main()