import json
import cv2 
from tqdm import tqdm

invalid_image_ids = []
split = 'train'
with open('/home/funkybirds/assignments/mlfin/flaw_data/'+split+'/'+split+'old.json', 'r') as file:
    data = json.load(file)
    for x in tqdm(data['images']):
        src = x['file_name']
        tmpl = x['template_name']
        img = cv2.imread('/home/funkybirds/assignments/mlfin/flaw_data/'+split+'/sample/'+src)
        tmpl_img = cv2.imread('/home/funkybirds/assignments/mlfin/flaw_data/'+split+'/template/'+tmpl)
        if img.shape != tmpl_img.shape or img.shape[0] != 400 or img.shape[1] != 400:
            invalid_image_ids.append(x['id'])
            print(x['id'])
            print(img.shape)
            print(tmpl_img.shape)
            print("Invalid Image",src,tmpl,img.shape,tmpl_img.shape)
    print("Invalid Image IDs",invalid_image_ids)

    valid_anno = []
    for x in tqdm(data['annotations']):
        if x['category_id'] == 0:
            data['annotations'].remove(x)
            print("Removed Annotation - Invalid Category",x)
            continue
        if x['image_id'] in invalid_image_ids:
            data['annotations'].remove(x)
            print("Removed Annotation - Invalid Image",x)
            continue
        if len(x['bbox']) == 4:
            if x['bbox'][0] < 0 or x['bbox'][1] < 0 or x['bbox'][2] < 0 or x['bbox'][3] < 0:
                data['annotations'].remove(x)
                print("Removed Annotation - Invalid Bbox",x)
                continue
        del x['segmentation']
        valid_anno.append(x)
    data['annotations'] = valid_anno
        
    
    # Reassign annotation id
    for i,x in enumerate(tqdm(data['annotations'])):
        data['annotations'][i]['id'] = i
 
# Save the file again
with open('/home/funkybirds/assignments/mlfin/flaw_data/'+split+'/'+split+'.json', 'w') as file:
    json.dump(data, file, indent=4)